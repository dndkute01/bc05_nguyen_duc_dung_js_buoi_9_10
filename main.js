var dsnv = [];
const DSNV = "DSNV";

function saveLocaStorage() {
  let jsonDsnv = JSON.stringify(dsnv);

  localStorage.setItem(DSNV, jsonDsnv);
}

var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var nvArr = JSON.parse(dataJson);
  for (index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new nhanVien(
      item.account,
      item.name,
      item.email,
      item.password,
      item.salary,
      item.day,
      item.hour,
      item.position
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv);
}

function addPn() {
  // get information from form
  var nv = getValueFromForm();

  var isValid = true;

  // validate account
  isValid =
    inspectEmpty(
      nv.account,
      "spanTaiKhoan",
      "Tên tài khoản không được để rỗng"
    ) &&
    inspectCharacter(
      nv.account,
      "spanTaiKhoan",
      "Tài khoản chứa tối đa 4-6 kí số"
    ) &&
    inspectDuplication(nv.account, dsnv);

  // validate name
  isValid =
    isValid &
      inspectEmpty(nv.name, "spanTenNV", "Tên nhân viên không được để rỗng") &&
    inspectLetters(nv.name, "spanTenNV", "Tên nhân viên phải là chữ");
  // validate email
  isValid =
    isValid &
      inspectEmpty(nv.email, "spanEmailNV", "Email không được để rỗng") &&
    inspectEmail(nv.email, "spanEmailNV", "Email không đúng định dạng");
  // validate password
  isValid =
    isValid &
      inspectEmpty(
        nv.password,
        "spanPasswordNV",
        "Password không được để rỗng"
      ) &&
    inspectPassword(
      nv.password,
      "spanPasswordNV",
      "Password từ 6-10 kí tự chứa ít nhất 1 kí tự số, 1 kí tự thường, 1 kí tự in hoa và 1 kí tự đặc biệt"
    );
  // validate salary
  isValid =
    isValid &
      inspectEmpty(nv.salary, "spanLuong", "Lương cơ bản không được để rỗng") &&
    inspectSalary(nv.salary, "spanLuong", "Lương cơ bản từ 1 triệu - 20 triệu");
  // validate hour
  isValid =
    isValid & inspectEmpty(nv.hour, "spanGioNV", "Giờ không được để rỗng") &&
    inspectHour(nv.hour, "spanGioNV", "Giờ làm từ 80-200 tiếng");
  // vadiate the rest
  isValid =
    isValid & inspectEmpty(nv.day, "spanNgayNV", "Ngày làm không được để rỗng");
  // validate

  if (isValid) {
    // push into list
    dsnv.push(nv);

    // save on localStorage

    saveLocaStorage();
    // Render  personnel list

    renderDsnv(dsnv);
    document.getElementById("txtTaiKhoan").disabled = false;
    resetForm();
  }
}

function removePn(id) {
  var location = findLocation(id, dsnv);

  if (location !== -1) {
    dsnv.splice(location, 1);
    // save localStorage
    saveLocaStorage();
    // render
    renderDsnv(dsnv);
  }
}

function editPn(id) {
  var location = findLocation(id, dsnv);

  if (location == -1) return;

  var data = dsnv[location];

  putValueOnForm(data);
  // prevent user edit account
  document.getElementById("txtTaiKhoan").disabled = true;
}

function updatePn(id) {
  var data = getValueFromForm();
  var location = findLocation(data.account, dsnv);
  if (location == -1) return;

  dsnv[location] = data;
  renderDsnv(dsnv);
  saveLocaStorage();
  // remove disabled
  document.getElementById("txtTaiKhoan").disabled = false;
  resetForm();
}
