function getValueFromForm(nv) {
  // get information from form

  var account = document.getElementById("txtTaiKhoan").value;
  var name = document.getElementById("txtTenNV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPassword").value;
  var salary = document.getElementById("txtLuong").value;
  var day = document.getElementById("txtNgay").value;
  var hour = document.getElementById("txtGio").value;
  var position = document.getElementById("txtChucVu").value;
  // create object nv
  var nv = new nhanVien(
    account,
    name,
    email,
    password,
    salary,
    day,
    hour,
    position
  );
  return nv;
}

function renderDsnv(nvArr) {
  var contentHTML = "";

  for (var index = 0; index < nvArr.length; index++) {
    var currentNv = nvArr[index];

    var contentTr = `
    <tr>
    <td>${currentNv.account}</td>
    <td>${currentNv.name}</td>
    <td>${currentNv.email}</td>
    <td>${currentNv.day}</td>
    <td>${currentNv.position}</td>
    <td>${currentNv.sum()}</td>
    <td>${currentNv.rank()}</td>
    <td>
    <i onclick ="removePn('${
      currentNv.account
    }')" class="fa fa-trash-alt text-danger" style="cursor: pointer;"></i>
    <i onclick ="editPn('${
      currentNv.account
    }')" class="fa fa-pen-fancy text-warning" style="cursor: pointer;"></i>
    </td>
    </tr>
     `;
    contentHTML += contentTr;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function rankHour() {
  for (var index = 0; index < nvArr.length; index++) {
    var itemHour = nvArr[index];

    if (itemHour.hour >= 192) {
    }
  }
}

function findLocation(id, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.account == id) {
      // return function
      return index;
    }
  }
  // if not find out, return -1
  return -1;
}

function putValueOnForm(data) {
  document.getElementById("txtTaiKhoan").value = data.account;
  document.getElementById("txtTenNV").value = data.name;
  document.getElementById("txtEmail").value = data.email;
  document.getElementById("txtPassword").value = data.password;
  document.getElementById("txtLuong").value = data.salary;
  document.getElementById("txtNgay").value = data.day;
  document.getElementById("txtGio").value = data.hour;
  document.getElementById("txtChucVu").value = data.position;
}

function resetForm() {
  document.getElementById("formQLNV").reset();
}

function showMessage(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}
