function inspectDuplication(id, dsnv) {
  var index = findLocation(id, dsnv);
  if (index == -1) {
    showMessage("spanTaiKhoan", "");
    return true;
  } else {
    showMessage("spanTaiKhoan", "Tài khoản đã tồn tại");
    return false;
  }
}

function inspectEmpty(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessage(idErr, message);
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
}

function inspectCharacter(value, idErr, message) {
  var reg = /^\d+$/;
  var isNumber = reg.test(value);
  if (value.length >= 4 && value.length <= 6 && isNumber == true) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}

function inspectPassword(value, idErr, message) {
  var reg = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,10}$)"
  );

  var isPassword = reg.test(value);
  console.log("isPassword: ", isPassword);

  if (isPassword) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}

function inspectLetters(value, idErr, message) {
  let isLetter = /^[a-zA-Z]+$/.test(value);
  if (isLetter) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}

function inspectEmail(value, idErr, message) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  if (isEmail) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}

function inspectSalary(value, idErr, message) {
  if (value >= 1000000 && value <= 20000000) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}

function inspectHour(value, idErr, message) {
  if (value >= 80 && value <= 200) {
    showMessage(idErr, "");
    return true;
  } else {
    showMessage(idErr, message);
    return false;
  }
}
