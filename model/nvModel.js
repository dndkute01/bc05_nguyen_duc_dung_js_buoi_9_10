function nhanVien(account, name, email, password, salary, day, hour, position) {
  this.account = account;
  this.name = name;
  this.email = email;
  this.password = password;
  this.salary = salary;
  this.day = day;
  this.hour = hour;
  this.position = position;

  if (this.position == "Giám đốc") {
    this.sum = function () {
      return (sum = this.salary * 3);
    };
  } else if (this.position == "Trưởng phòng") {
    this.sum = function () {
      return (sum = this.salary * 2);
    };
  } else {
    this.sum = function () {
      return (sum = this.salary);
    };
  }

  if (this.hour >= 192) {
    this.rank = function () {
      return (rank = " Xuất sắc");
    };
  } else if (this.hour >= 176) {
    this.rank = function () {
      return (rank = " Giỏi");
    };
  } else if (this.hour >= 160) {
    this.rank = function () {
      return (rank = " Khá");
    };
  } else {
    this.rank = function () {
      return (rank = " Trung bình");
    };
  }
}
